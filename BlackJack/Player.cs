﻿using System.Collections.Generic;

namespace BlackJack
{
    class Player
    {
        static int players_number_static = 0;
        private List<Card> cards;
        private int score;
        private bool no_more;
        private int player_name;
        private int money;

        public Player(bool take_name = true,int money = 1000)
        {
            no_more = false;
            this.cards = new List<Card>();
            this.score = 0;
            this.money = money;
            if (take_name == true)
            {
                player_name = ++players_number_static;
            }
            else
            {
                player_name = 0;
            }


        }

        public int Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }

        public List<Card> Cards
        {
            get
            {
                return this.cards;
            }
        }

        public int Player_name
        {
            get
            {
                return player_name;
            }
        }

        public int Score
        {
            get
            {
                return this.score;
            }
            set
            {
                if (value >= 0)
                {
                    score = value;
                }
            }
        }

        public bool No_more
        {
            get
            {
                return this.no_more;
            }
            set
            {
                no_more = value;
            }
        }
    }
}
