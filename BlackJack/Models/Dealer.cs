﻿using System.Collections.Generic;

namespace BlackJack
{
    class Dealer
    {
        public List<Card> Cards { get; set; }
        public int Score { get; set; }
        public bool NoMore { get; set; }
        public int DealerName { get; set; }
    }
}
