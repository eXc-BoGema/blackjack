﻿
namespace BlackJack
{
    class Card
    {
        public Value CardValue { get; set; }
        public Suit Suit { get; set; }
    }
}
