﻿using System.Collections.Generic;

namespace BlackJack
{
    class Player
    {
        public List<Card> Cards { get; set; }
        public int Score { get; set; }
        public bool NoMore { get; set; }
        public int PlayerName { get; set; }
        public int Money { get; set; }
    }
}
