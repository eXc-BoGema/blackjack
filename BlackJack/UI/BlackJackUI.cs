﻿using System;
using System.Collections.Generic;

namespace BlackJack
{
    class BlackJackUI
    {
        public void ShowOnStart()
        {
            Console.WriteLine("Добро пожаловать в BlackJack, введите кол-во игроков(цифрой, от 1. В случае ошибки будет установлен один игрок):");
        }

        public void ShowPlayersChoiceForMove(int player)
        {
            Console.WriteLine("ход игрока {0}, введите 1 для получения новой карты, 2 - закончить ход", player);
        }

        public void ShowMoveResults(Card takenCard, int playersScore)
        {
            Console.WriteLine("{0}\nСчет теперь: {1}", GetNameOfTheCard(takenCard), playersScore);
        }

        public void ShowPlayerPassed()
        {
            Console.WriteLine("Не взял карту");
        }

        public void ShowPlayAgainQuestion()
        {
            Console.WriteLine("играть еще? 1-да, 2- нет");
        }

        public void ShowDealerTakeCards()
        {
            Console.WriteLine("Дилер сдает себе...");
        }

        public void ShowAllLoose()
        {
            Console.WriteLine("Все проиграли, ставки забирает казино");
        }

        public string GetNameOfTheCard(Card card)
        {
            return card.CardValue + " " + card.Suit;
        }

        public void ShowResultsOfRound(List<Player> players, int[] rates)
        {
            foreach (var winner in players)
            {
                Console.WriteLine("Победитель {0}", winner.PlayerName);
                players[winner.PlayerName - 1].Money += rates[winner.PlayerName - 1] * 2;
                Console.WriteLine("Выигрыш: {0}, денег на счету -  {1}", rates[winner.PlayerName - 1], players[winner.PlayerName - 1].Money);
            }
        }

        public void ShowAllPlayers(Dealer dealer, List<Player> players, int[] rates)
        {
            Console.WriteLine("Дилер");
            foreach (var card in dealer.Cards)
            {
                Console.WriteLine(GetNameOfTheCard(card));
            }
            Console.WriteLine("Счет: {0}\n", dealer.Score);

            foreach (var player in players)
            {
                Console.WriteLine("Игрок {0}", player.PlayerName);
                Console.WriteLine("Ставка: {0}", rates[player.PlayerName - 1]);
                foreach (var card in player.Cards)
                {
                    Console.WriteLine(GetNameOfTheCard(card));
                }
                Console.WriteLine("Счет: {0}\n", player.Score);

            }
        }

        public string GetPlayerChoice()
        {
            string choice = "";
            for (bool exit = false; !exit;)
            {
                try
                {
                    choice = Console.ReadLine();
                    Convert.ToInt16(choice);
                    exit = true;
                }
                catch
                {
                    Console.WriteLine("Ошибка ввода");
                }
            }
            return choice;
        }

        public void ShowOnBets(int playersName)
        {
            Console.WriteLine("Ставка игрока {0}: 1 - ставка х1, 2 - x2, 3 - x3", playersName);
        }
    }
}