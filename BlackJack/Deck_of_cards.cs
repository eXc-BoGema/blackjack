﻿using System;
using System.Collections.Generic;

namespace BlackJack
{
    class Deck_of_cards
    {
        private List<Card> cards;
        private int usaged_cards;
        private Random rng;

        public Deck_of_cards()
        {
            rng = new Random((int)DateTime.Now.Ticks);
            usaged_cards = 0;
            cards = new List<Card>();

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    cards.Add(new Card((Value)j, (Suit)i));
                }
            }
        }

        public List<Card> Cards
        {
            get
            {
                return cards;
            }
        }

        public Card Get_card()
        {
            try
            {
                int temp = rng.Next(51 - usaged_cards++);
                Card temp_card = cards[temp];
                cards.RemoveAt(temp);
                return temp_card;
            }
            catch
            {
                Console.WriteLine("Закончилась колода");
                return new Card(Value.Jack, Suit.Diamond);//временно
            }
        }

    }
}
