﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackJack
{
    class Game
    {
        Dealer dealer;
        private List<Player> players;
        private Deck_of_cards deck;
        private int whos_turn;
        private int[] rates;

        public Game(int num_of_players)
        {
            dealer = new Dealer();
            players = new List<Player>();
            for (int i = 0; i < num_of_players; i++)
            {
                players.Add(new Player());
            }
            rates = new int[players.Count];

            deck = new Deck_of_cards();
            whos_turn = 0;
            
        }

        public Dealer Dealer
        {
            get
            {
                return dealer;
            }
        }

        public List<Player> Players
        {
            get
            {
                return players;
            }
        }

        public Deck_of_cards Deck
        {
            get
            {
                return deck;
            }
        }

        private void Player_move()
        {
            string choice;
            Console.WriteLine("ход игрока {0}, введите 1 для получения новой карты, 2 - закончить ход",players[whos_turn].Player_name);
                for (bool exit = false; !exit;)
                {
                    choice = Console.ReadLine();

                try
                {
                    switch (Convert.ToUInt64(choice))
                    {
                        case 1:
                            players[whos_turn].Cards.Add(deck.Get_card());
                            players[whos_turn].Score += players[whos_turn].Cards.Last().GetCost(players[whos_turn].Score);
                            Console.WriteLine("{0}\nСчет теперь: {1}", players[whos_turn].Cards.Last().ToString(), players[whos_turn].Score);
                            Turn();
                            exit = true;
                            break;
                        case 2:
                            Console.WriteLine("Не взял карту");
                            players[whos_turn].No_more = true;
                            Turn();
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Error");
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Ошибка ввода");
                }
            }
        }

        private void Make_bets()
        {
            for (int i = 0; i < players.Count; i++)
            {
                rates[i] = 0;
                for (bool exit = false; !exit;)
                {
                    Console.WriteLine("Ставка игрока {0}: 1 - ставка х1, 2 - x2, 3 - x3",players[i].Player_name);
                    string choice = Console.ReadLine();
                    try
                    {
                        switch (Convert.ToInt16(choice))
                        {
                            case 1:
                                rates[i] = new Rate().Make_a_bet();
                                players[i].Money -= rates[i];
                                exit = true;
                                break;
                            case 2:
                                rates[i] = new Rate_x2().Make_a_bet();
                                players[i].Money -= rates[i];
                                exit = true;
                                break;
                            case 3:
                                rates[i] = new Rate_x3().Make_a_bet();
                                players[i].Money -= rates[i];
                                exit = true;
                                break;
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Ошибка ввода");
                    }
                }
            }
        }

        public void Game_start()
        {
            dealer.Cards.Add(deck.Get_card());
            dealer.Score += dealer.Cards[0].GetCost(dealer.Score);

            for (int i = 0; i < 2; i++)
            {
                foreach (var player in players)
                {
                    player.Cards.Add(deck.Get_card());
                    player.Score += player.Cards.Last().GetCost(player.Score);
                }
            }

            Show();
            Make_bets();

            dealer.Cards.Add(deck.Get_card());
            dealer.Score += dealer.Cards[1].GetCost(dealer.Score);

            bool[] check_players = new bool[players.Count];

            for (bool exit = false; !exit;)
            {
                if (players[whos_turn].No_more)
                {
                    check_players[whos_turn] = true;
                    Turn();
                }
                else
                {
                    Player_move();
                }

                if (!check_players.Contains(false))
                {
                    exit = true;
                }
            }

            Dealer_to_himself();
            Show();
            Results();
            Play_again();
        }
                
        private void Play_again()
        {
            Console.WriteLine("играть еще? 1-да, 2- нет");
        again:
            string choice = Console.ReadLine();
            try
            {
                if (Convert.ToUInt64(choice) == 1)
                {
                    dealer.No_more = false;
                    dealer.Score = 0;
                    dealer.Cards.Clear();
                    for (int i = 0; i < players.Count; i++)
                    {
                        players[i].Score = 0;
                        players[i].Cards.Clear();
                        players[i].No_more = false;
                    }
                    rates = new int[players.Count];
                    deck = new Deck_of_cards();
                    whos_turn = 0;

                    Game_start();
                }
            }
            catch
            {
                Console.WriteLine("Ошибка ввода");
                goto again;
            }
        }

        private void Dealer_to_himself()
        {
            Console.WriteLine("Дилер сдает себе...");
            do
            {
                if (dealer.Score < 17)
                {
                    dealer.Cards.Add(deck.Get_card());
                    dealer.Score += dealer.Cards.Last().GetCost(dealer.Score);
                }
                else
                {
                    dealer.No_more = true;
                }
            } while (!dealer.No_more);
        }

        private void Show()
        {
            Console.WriteLine("Дилер");
            foreach(var card in dealer.Cards)
            {
                Console.WriteLine(card.ToString());
            }
            Console.WriteLine("Счет: {0}\n", dealer.Score);

            foreach (var player in players)
            {
                Console.WriteLine("Игрок {0}", player.Player_name);
                Console.WriteLine("Ставка: {0}",rates[player.Player_name-1]);
                foreach (var card in player.Cards)
                {
                    Console.WriteLine(card.ToString());
                }
                Console.WriteLine("Счет: {0}\n", player.Score);

            }
        }

        private void Results()
        {
            if (players.FindAll(x => x.Score == 21 && dealer.Score == 21).Count > 0)
            {
                foreach (var winner in players.FindAll(x => x.Score == 21))
                {
                    Console.WriteLine("Победитель {0}", winner.Player_name);
                    players[winner.Player_name -1].Money += rates[winner.Player_name - 1];
                    Console.WriteLine("Выигрыш: {0}, денег на счету -  {1}", rates[winner.Player_name - 1], players[winner.Player_name - 1].Money);
                }
            }
            else if (players.FindAll(x => x.Score <= 21 && x.Score > dealer.Score && dealer.Score < 21).Count > 0)
            {
                var winners = players.FindAll(x => x.Score <= 21 && x.Score > dealer.Score);
                foreach (var winner in winners)
                {
                    Console.WriteLine("Победитель {0}", winner.Player_name);
                    players[winner.Player_name - 1].Money += rates[winner.Player_name - 1]*2;
                    Console.WriteLine("Выигрыш: {0}, денег на счету -  {1},", rates[winner.Player_name - 1]*2, players[winner.Player_name - 1].Money);
                }
            }
            else if (players.FindAll(x => x.Score <= 21 && dealer.Score > 21).Count > 0)
            {
                var winners = players.FindAll(x => x.Score <= 21);
                foreach (var winner in winners)
                {
                    Console.WriteLine("Победитель {0}", winner.Player_name);
                    players[winner.Player_name - 1].Money += rates[winner.Player_name - 1] * 3;
                    Console.WriteLine("Выигрыш: {0}, денег на счету -  {1},", rates[winner.Player_name - 1]*3, players[winner.Player_name - 1].Money);
                }
            }
            else
            {
                Console.WriteLine("Все проиграли, ставки забирает казино");
            }

        }

        private void Turn()
        {
            if (whos_turn < this.players.Count-1)
                whos_turn++;
            else whos_turn = 0;
        }


    }
}
