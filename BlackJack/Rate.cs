﻿
namespace BlackJack
{
    interface IRates
    {
        int Make_a_bet();
    }

    class Rate : IRates
    {
        static int rate_x1 = 50;

        public int Make_a_bet()
        {
            return rate_x1;
        }
    }
}
