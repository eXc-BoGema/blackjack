﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackJack
{
    class Game
    {
        private BlackJackUI _blackJackUi; 
        private Dealer _dealer;
        private Bet _bet;
        private List<Player> _players;
        private DeckOfCards _deck;
        private int _whosTurn;
        private int[] _rates;
        private Random _rng;
        private int _playersNumber;

        public Game(int numOfPlayers)
        {
            _bet = new Bet() {StockBet = 50 };
            _blackJackUi = new BlackJackUI();
            _dealer = new Dealer() { DealerName = _playersNumber++, Score = 0, NoMore = false, Cards = new List<Card>() };
            _players = new List<Player>();
            for (int i = 0; i < numOfPlayers; i++)
            {
                _players.Add(new Player() { PlayerName = _playersNumber++, Cards = new List<Card>(), Money = 1000, NoMore = false, Score = 0 });
            }
            _rates = new int[_players.Count];

            _deck = new DeckOfCards();
            _whosTurn = 0;
            _playersNumber = 0;
            _rng = new Random((int)DateTime.Now.Ticks);
            _deck.Cards = new List<Card>();
            FillCardDeck();
        }

        private void MoveOfPlayer()
        {
            string choice;
            _blackJackUi.ShowPlayersChoiceForMove(_players[_whosTurn].PlayerName);
            choice = _blackJackUi.GetPlayerChoice();

            if (Convert.ToUInt16(choice) == 1)
            {
                _players[_whosTurn].Cards.Add(GetCard());
                _players[_whosTurn].Score += GetCost(_players[_whosTurn].Cards.Last(), _players[_whosTurn].Score);
                _blackJackUi.ShowMoveResults(_players[_whosTurn].Cards.Last(), _players[_whosTurn].Score);
                ChangeTurn();
                return;
            }
            if (Convert.ToUInt16(choice) == 2)
            {
                _blackJackUi.ShowPlayerPassed();
                _players[_whosTurn].NoMore = true;
                ChangeTurn();
                return;
            }
            Console.WriteLine("Error");
            MoveOfPlayer();
        }

        private void MakeBets()
        {
            for (int i = 0; i < _players.Count;)
            {
                _rates[i] = 0;
                _blackJackUi.ShowOnBets(_players[i].PlayerName);
                string choice = _blackJackUi.GetPlayerChoice();

                if (Convert.ToInt16(choice) == 1)
                {
                    _rates[i] = GetBet(1);
                    _players[i].Money -= _rates[i];
                    ++i;
                }
                if (Convert.ToInt16(choice) == 2)
                {
                    _rates[i] = GetBet(2);
                    _players[i].Money -= _rates[i];
                    ++i;
                }
                if (Convert.ToInt16(choice) == 3)
                {
                    _rates[i] = GetBet(3);
                    _players[i].Money -= _rates[i];
                    ++i;
                }

            }
        }

        public void StartGame()
        {
            _dealer.Cards.Add(GetCard());
            _dealer.Score += GetCost(_dealer.Cards[0],_dealer.Score);

            for (int i = 0; i < 2; i++)
            {
                foreach (var player in _players)
                {
                    player.Cards.Add(GetCard());
                    player.Score += GetCost(player.Cards.Last(),player.Score);
                }
            }

            _blackJackUi.ShowAllPlayers(_dealer,_players,_rates);

            MakeBets();

            _dealer.Cards.Add(GetCard());
            _dealer.Score += GetCost(_dealer.Cards[1],_dealer.Score);

            //
            bool[] checkPlayers = new bool[_players.Count];

            for (bool exit = false; !exit;)
            {
                if (_players[_whosTurn].NoMore)
                {
                    checkPlayers[_whosTurn] = true;
                    ChangeTurn();
                }
                if (!_players[_whosTurn].NoMore)
                {
                    MoveOfPlayer();
                }

                if (!checkPlayers.Contains(false))
                {
                    exit = true;
                }
            }

            TakeCardsForDealer();
            _blackJackUi.ShowAllPlayers(_dealer, _players, _rates);
            ShowResults();
            PlayAgain();
        }

        private void PlayAgain()
        {
            _blackJackUi.ShowPlayAgainQuestion();
            string choice = _blackJackUi.GetPlayerChoice();

            if (Convert.ToInt16(choice) != 1)
            {
                return;
            }
                _dealer.NoMore = false;
                _dealer.Score = 0;
                _dealer.Cards.Clear();
                for (int i = 0; i < _players.Count; i++)
                {
                    _players[i].Score = 0;
                    _players[i].Cards.Clear();
                    _players[i].NoMore = false;
                }
                _rates = new int[_players.Count];
                FillCardDeck();
                _deck.UsagedCards = 0;
                _whosTurn = 0;
                StartGame();
        }

        private void TakeCardsForDealer()
        {
            _blackJackUi.ShowDealerTakeCards();
            int _dealerMinScore = 17;
            for (; _dealer.Score < _dealerMinScore;)
            {
                _dealer.Cards.Add(GetCard());
                _dealer.Score += GetCost(_dealer.Cards.Last(), _dealer.Score);
            }
        }

        private void ShowResults()
        {
            if (_players.FindAll(x => x.Score == 21 && _dealer.Score == 21).Count > 0)
            {
                var winners = _players.FindAll(x => x.Score == 21);

                _blackJackUi.ShowResultsOfRound(winners, _rates);
            }
            else if (_players.FindAll(x => x.Score <= 21 && x.Score > _dealer.Score && _dealer.Score < 21).Count > 0)
            {
                var winners = _players.FindAll(x => x.Score <= 21 && x.Score > _dealer.Score);
                _blackJackUi.ShowResultsOfRound(winners, _rates);
            }
            else if (_players.FindAll(x => x.Score <= 21 && _dealer.Score > 21).Count > 0)
            {
                var winners = _players.FindAll(x => x.Score <= 21);
                _blackJackUi.ShowResultsOfRound(winners, _rates);
            }
            else
            {
                _blackJackUi.ShowAllLoose();
            }
        }

        private void ChangeTurn()
        {
            if (_whosTurn < this._players.Count-1)
                _whosTurn++;
            else _whosTurn = 0;
        }

        public int GetCost(Card card,int _players_score)
        {
            if(card.CardValue == Value.Ace)
            {
                if (_players_score < 21)
                {
                    return 11;
                }
                return 1;
            }
            if(card.CardValue == Value.Jack ||
               card.CardValue == Value.Queen ||
               card.CardValue == Value.King)
            {
                return 10;
            }
            return (int)card.CardValue + 1;
        }

        public Card GetCard()
        {
                int temp = _rng.Next(51 - _deck.UsagedCards++);
                Card tempCard = _deck.Cards[temp];
                _deck.Cards.RemoveAt(temp);
                return tempCard;
        }

        public void FillCardDeck()
        {
            _deck.Cards.Clear();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    _deck.Cards.Add(new Card() { CardValue = (Value)j, Suit = (Suit)i });
                }
            }
        }

        public int GetBet(int modifier)
        {
            return _bet.StockBet * modifier;
        }

    }
}
