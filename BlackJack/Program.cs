﻿using System;

namespace BlackJack
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice;
            BlackJackUI Ui = new BlackJackUI();
            Ui.ShowOnStart();
            choice = Ui.GetPlayerChoice();
            Game game;
            if (Convert.ToInt32(choice) > 0 && Convert.ToInt32(choice) < 5)
            {
                game = new Game(Convert.ToInt32(choice));
            }
            else
            {
                game = new Game(1);
            }

            game.StartGame();
        }

    }
}
