﻿
namespace BlackJack
{
    public enum Suit
    {
        Heart,
        Diamond,
        Spade,
        Club
    }

    public enum Value
    {
        Ace,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King
    }



    class Card
    {
        private Value card_value;
        private Suit suit;

        public Card(Value card_value, Suit suit)
        {
            this.Card_value = card_value;
            this.Suit = suit;
        }

        public Value Card_value
        {
            get
            {
                return card_value;
            }
            set
            {
                if (value >= (Value)0 && value <= (Value)12)
                {
                    this.card_value = value;
                }
            }
        }
        public Suit Suit
        {
            get
            {
                return suit;
            }
            set
            {
                if(value >= (Suit)0 && value <= (Suit)4)
                suit = value;
            }
        }

        public int GetCost(int players_score)
        {
            switch (card_value)
            {
                case Value.Ace:
                    if (players_score < 21)
                    {
                        return 11;
                    }
                    return 1;
                case Value.Jack:
                case Value.Queen:
                case Value.King:
                    return 10;
                default:
                    return (int)card_value + 1;
            }
        }


        public override string ToString()
        {
            return card_value + " " + suit;
        }
        

    }
}
